<?php
/**
 * @file
 * Install and uninstall functions for the Dilve module.
 */


/**
 * Implements hook_install().
 */
function dilve_install() {
  $t = get_t();

  // Define the node type.
  $node_dilve_book = array(
    'type' => 'dilve_book',
    'name' => $t('Dilve-book'),
    'base' => 'node_content',
    'description' => $t('Information on a (physical) book based on its ISBN.'),
  );

  // Complete the node type definition by setting any defaults not explicitly
  // declared above.
  // http://api.drupal.org/api/function/node_type_set_defaults/7
  $content_type = node_type_set_defaults($node_dilve_book);
  // Omit the body field for now.
  node_add_body_field($content_type);

  // Save the content type.
  node_type_save($content_type);

  // Create all the fields we are adding to our content type.
  // http://api.drupal.org/api/function/field_create_field/7
  foreach (_dilve_installed_fields() as $field) {
    field_create_field($field);
  }

  // Create all the instances for our fields.
  // http://api.drupal.org/api/function/field_create_instance/7
  foreach (_dilve_installed_instances() as $instance) {
    $instance['entity_type'] = 'node';
    $instance['bundle'] = $node_dilve_book['type'];
    field_create_instance($instance);
  }

}


/**
 * Return an array of supplementary fields needed for dilve_book.
 */
function _dilve_installed_fields() {

  return array(
    'dilve_isbn' => array(
      'field_name' => 'dilve_isbn',
      'cardinality' => 1,
      'type' => 'text',
      'settings' => array(
        'max_length' => 15,
      ),
    ),

    'dilve_language' => array(
      'field_name' => 'dilve_language',
      'cardinality' => 1,
      'type' => 'text',
    ),

    'dilve_author' => array(
      'field_name' => 'dilve_author',
      'cardinality' => 1,
      'type' => 'text',
    ),

    'dilve_cover' => array(
      'field_name' => 'dilve_cover',
      'cardinality' => 1,
      'type' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
    ),

    'dilve_publisher' => array(
      'field_name' => 'dilve_publisher',
      'cardinality' => 1,
      'type' => 'text',
    ),

    'dilve_num_pages' => array(
      'field_name' => 'dilve_num_pages',
      'cardinality' => 1,
      'type' => 'text',
    ),

  );
}

/**
 * Returns a structured array defining the instances for this content type.
 *
 * This is provided as a function so that it can be used in both hook_install()
 * and hook_uninstall().
 */
function _dilve_installed_instances() {
  $t = get_t();
  return array(

    'dilve_isbn' => array(
      'field_name' => 'dilve_isbn',
      'label' => $t('ISBN'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),
    'dilve_cover' => array(
      'field_name' => 'dilve_cover',
      'label' => $t('Cover image'),
      'cardinality' => 1,
      'type' => 'article_image',
      'settings' => array(
        'alt_field' => 1,
        'title_field' => 1,
        'file_directory' => 'image',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '',
        'min_resolution' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'settings' => array(
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ),
      ),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'image',
          'settings' => array('image_style' => 'medium', 'image_link' => 'file'),
          'weight' => -1,
        ),
        'teaser' => array(
          'label' => 'hidden',
          'type' => 'image',
          'settings' => array('image_style' => 'thumbnail', 'image_link' => 'content'),
          'weight' => -1,
        ),
      ),
    ),

    'dilve_language' => array(
      'field_name' => 'dilve_language',
      'label' => $t('Language'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),

    'dilve_author' => array(
      'field_name' => 'dilve_author',
      'label' => $t('Author'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),

    'dilve_publisher' => array(
      'field_name' => 'dilve_publisher',
      'label' => $t('Publisher'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),

    'dilve_num_pages' => array(
      'field_name' => 'dilve_num_pages',
      'label' => $t('Pages'),
      'widget' => array(
        'type' => 'text_textfield',
      ),
    ),

  );
}


/**
 * Implements hook_uninstall().
 */
function dilve_uninstall() {
  variable_del('dilve_db');
  variable_del('dilve_user');
  variable_del('dilve_password');

  // Gather all the example content that might have been created while this
  // module was enabled.  Simple selects still use db_query().
  // http://api.drupal.org/api/function/db_query/7
  /* $sql = 'SELECT nid FROM {node} n WHERE n.type = :type'; */
  /* $result = db_query($sql, array(':type' => 'dilve_book')); */

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'dilve_book');

  $result = $query->execute();

  $nids = array();
  foreach ($result['node'] as $row) {
    $nids[] = $row->nid;
  }

  // Delete all the nodes at once
  // http://api.drupal.org/api/function/node_delete_multiple/7
  node_delete_multiple($nids);

  // Loop over each of the fields defined by this module and delete
  // all instances of the field, their data, and the field itself.
  // http://api.drupal.org/api/function/field_delete_field/7
  foreach (array_keys(_dilve_installed_fields()) as $field) {
    field_delete_field($field);
  }

  // Loop over any remaining field instances attached to the node_example
  // content type (such as the body field) and delete them individually.
  // http://api.drupal.org/api/function/field_delete_field/7
  $instances = field_info_instances('node', 'dilve_book');
  foreach ($instances as $instance_name => $instance) {
    field_delete_instance($instance);
  }

  // Delete our content type
  // http://api.drupal.org/api/function/node_type_delete/7
  node_type_delete('dilve_book');

  // Purge all field information
  // http://api.drupal.org/api/function/field_purge_batch/7
  field_purge_batch(1000);
}
