dilve 1.1 for Drupal 7.x
------------------------------

This is a Drupal 7 module that will create nodes in a book archive based on
ISBN's. ISBN's or International Standard Book Numbers are unique identifiers for
books, and based on ISBN you can mostly gather quite a bit of information about
a book as well as get a hold of its cover. The module will allow users to easily
build archives of physical books based on these book's ISBN's.

DILVE is the largest catalog of books published in Spain. http://www.dilve.es/
DILVE uses the international standard ONIX, based on XML.


The module provides
------------------------------

- a basic ISBN-based book content type (not to confuse with Drupal's own book
    content type).
- a mechanism to fetch the book information from DILVE through
    their API's. They have information on millions of titles.
- a mechanism to add many books at once through a list of ISBN's.
- a simple form to set up the module, access keys etc.

The content type for this module is called an DILVE-book (bilve_book) to
separate it from Drupal's built-in book content type. If you want a less nerdy
name for the content type, you can rename it under
admin -> structure -> types -> manage -> dilve-book if you please.

The content type contains these fields:

- Title
- Cover image
- ISBN
- Author
- Publisher
- Body
- Num pages
- Language

The fields will be matched with fields in the online-databases as best possible.
The databases do not follow a common standard, but have almost similar
information.

Once the module is installed, you can of course extend the content type with 
your own fields, add taxonomies, manipulate its display settings, use its 
fields in views etc.

ISBN or Title addition
------------------------------

You add a book based on ISBN or Title by selecting
node -> add -> Search by ISBN or Title in DILVE and enter an ISBN or Title.
The module will then try to fetch the information on the book 
and upon success populate a node form
for the book, which you can then edit (or leave as it is) and save.

The module will check

- whether the book can be found based on ISBN
- whether the book is already in the system based on its ISBN

Failing in either situation will spawn an error and allow you to take action.


Bulk import

You can bulk import books by going to node -> add -> dilve_book/bulk and
enter a list of ISBN's, one on each line. The module will then create an
book for each number without a previous editing option, but skip ISBN's
that give no results and books already in the system based on ISBN.


Future
------------------------------

But there are still some caveats and to-do's:

- Support several ISBN-databases simultaneously
Now you choose one or the other, but the optimal would be to be able to choose
and prioritize several. That would allow the module to search one, and upon
failure continue to the next. Many books exist in one database and not the
other, and this would potentially give more successes.

- D8 version
It's time to move on!


Thanks
------------------------------

Dilve module is based on http://drupal.org/project/ISBN2node 
developed by vertikal.dk http://drupal.org/user/81257
To carlos.burgueño http://drupal.org/user/1122042 for your time.
